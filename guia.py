import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class Mainwindow(Gtk.Window):
    def __init__(self):
        super().__init__()

        self.set_title("HeaderBar")
        self.set_default_size(-1, 200)
        self.connect("destroy", Gtk.main_quit)

        headerbar = Gtk.HeaderBar()
        headerbar.set_title("Guia recuperativa")
        headerbar.set_decoration_layout("menu:minimize,maximize,close")
        headerbar.set_show_close_button(True)
        self.set_titlebar(headerbar)

        self.box = Gtk.Box()
        self.box.set_orientation(Gtk.Orientation.VERTICAL)
        self.add(self.box)

        self.nombre1 = Gtk.Label(label="Palabra 1")
        self.box.add(self.nombre1)
        self.entry1 = Gtk.Entry()
        self.entry1.connect("changed", self.calculo)
        self.box.add(self.entry1)

        self.nombre2 = Gtk.Label(label="Palabra 2")
        self.box.add(self.nombre2)
        self.entry2 = Gtk.Entry()
        self.entry2.connect("changed", self.calculo)
        self.box.add(self.entry2)

        self.nombre3 = Gtk.Label(label="Resultado")
        self.box.add(self.nombre3)
        self.pantalla = Gtk.TextView()
        self.pantalla.set_editable(False)
        self.box.add(self.pantalla)

        self.combobox = Gtk.ComboBox
        self.boton1 = Gtk.Button(label ="Aceptar")
        self.boton1.connect("clicked", self.aceptar_apretado)
        self.box.add(self.boton1)

        self.boton2 = Gtk.Button(label="Guardar")
        self.boton2.connect("clicked", self.guardar_apretado)
        self.box.add(self.boton2)

        self.boton3 = Gtk.Button(label="Reiniciar")
        self.boton3.connect("clicked", self.reiniciar_apretado)
        self.box.add(self.boton3)

    def calculo(self, btn = None):
        texto1 = self.entry1.get_text()
        texto2 = self.entry2.get_text()
        contador1 = 0
        contador2 = 0

        if texto1.isnumeric() == True and texto2.isnumeric() == True:
            texto1_num = int(texto1)
            texto2_num = int(texto2)
            texto1 = texto1 + " + "    
            resultado = str(texto1_num + texto2_num)
            self.contenido = self.pantalla.get_buffer()
            self.contenido.set_text(resultado)

        elif texto1.isnumeric() == False and texto2.isnumeric() == False:
            
            for i in range(len(texto1)):
                texto = texto1[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador1 += 1
            
            for i in range(len(texto2)):
                texto = texto2[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador2 += 1

            texto1 = texto1 + " + " 
            resultado = str(contador1 + contador2) + " Vocales"
            self.contenido = self.pantalla.get_buffer()
            self.contenido.set_text(resultado)
        
    def aceptar_apretado(self, btn = None):
        texto1 = self.entry1.get_text()
        texto2 = self.entry2.get_text()
        contador1 = 0
        contador2 = 0

        if texto1.isnumeric() == True and texto2.isnumeric() == True:
            texto1_num = int(texto1)
            texto2_num = int(texto2)
            texto1 = texto1 + " + "    
            resultado = str(texto1_num + texto2_num)
            self.contenido = self.pantalla.get_buffer()
            self.contenido.set_text(resultado)

        elif texto1.isnumeric() == False and texto2.isnumeric() == False:
            
            for i in range(len(texto1)):
                texto = texto1[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador1 += 1
            
            for i in range(len(texto2)):
                texto = texto2[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador2 += 1

            texto1 = texto1 + " + " 
            resultado = str(contador1 + contador2) + " Vocales"
            self.contenido = self.pantalla.get_buffer()
            self.contenido.set_text(resultado)
        
        else:
            texto1 = "LOS DATOS NO COINCIDEN"
            texto2 = ""
            resultado = ""
        
            
        dialogo_aceptar = Gtk.MessageDialog(
        transient_for =self,
        flags = 0,
        message_type = Gtk.MessageType.INFO,
        text = texto1 + texto2,
        secondary_text = resultado,
        buttons = Gtk.ButtonsType.OK)

        respuesta = dialogo_aceptar.run()
        if respuesta == Gtk.ResponseType.OK:
            dialogo_aceptar.destroy()

        dialogo_aceptar.destroy()

    def guardar_apretado(self,btn = None):
        dialog = Gtk.FileChooserDialog(title="Seleccione pavanzada.txt para guardar", 
                                       parent=self, 
                                       action=Gtk.FileChooserAction.SAVE)
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE,
            Gtk.ResponseType.OK,)
        
        texto1 = self.entry1.get_text()
        texto2 = self.entry2.get_text()
        contador1 = 0
        contador2 = 0

        if texto1.isnumeric() == True and texto2.isnumeric() == True:
            texto1_num = int(texto1)
            texto2_num = int(texto2)
              
            resultado = str(texto1_num + texto2_num)

        elif texto1.isnumeric() == False and texto2.isnumeric() == False:
                    
            for i in range(len(texto1)):
                texto = texto1[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador1 += 1
                    
            for i in range(len(texto2)):
                texto = texto2[i].lower()
                if texto == "a" or texto == "e" or texto == "i" or texto == "o" or texto == "u":
                    contador2 += 1

                
                resultado = str(contador1 + contador2) + " Vocales"

        nueva_linea = texto1 + " " + texto2 + " " + resultado + "\n"

        dialog.set_filename("pavanzada.txt")
        self.add_filters(dialog)

        response = dialog.run()
        

        if response == Gtk.ResponseType.OK:
            with open("pavanzada.txt", 'a') as archivo:
                archivo.write(nueva_linea)
            
            dialog.destroy()


        elif response == Gtk.ResponseType.CANCEL:
            dialog.destroy()

        dialog.destroy()
    
    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

    def reiniciar_apretado(self, btn = None):
        texto1 = self.entry1.get_text()
        texto2 = self.entry2.get_text()
        
        dialogo_reiniciar = Gtk.MessageDialog(
        transient_for =self,
        flags = 0,
        message_type = Gtk.MessageType.WARNING,
        buttons = Gtk.ButtonsType.OK_CANCEL,
        text = "ESTA SEGURO QUE QUIERE REINICIAR?",
        secondary_text = "Palabra 1:" + texto1 + " Palabra 2:" + texto2)

        respuesta = dialogo_reiniciar.run()
        if respuesta == Gtk.ResponseType.OK:
            self.entry1.set_text("")
            self.entry2.set_text("")
            self.contenido = self.pantalla.get_buffer()
            self.contenido.set_text("")
            dialogo_reiniciar.destroy()
        elif respuesta == Gtk.ResponseType.CANCEL:
            dialogo_reiniciar.destroy()

        dialogo_reiniciar.destroy()
        
        pass  

win = Mainwindow()
win.resize(375, 245)
win.show_all()
Gtk.main()
